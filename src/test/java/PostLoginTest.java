import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class PostLoginTest {
    private final String token = "f8c86a8b46a2ec6b76c2cbd2968ad21f";
    private final String username = "5165";
    private final String password = "9c22c0b51b";
    private final String loginURL = "https://test-stand.gb.ru/gateway/login";

    @Test
    void postLoginTest() {
        final String response = given()
                .formParam("username", username)
                .formParam("password", password)
                .log()
                .all()
                .when()
                .post(loginURL)
                .prettyPeek()
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .asString();
        assertThat(response, containsString("token"));
    }

    @Test
    void negativePostLoginTest() {
        final String response =
                given()
                        .formParam("username", "sv")
                        .formParam("password", "sv")
                        .log()
                        .all()
                        .when()
                        .post(loginURL)
                        .prettyPeek()
                        .then()
                        .assertThat()
                        .statusCode(200)
                        .extract()
                        .asString();
        assertThat(response, containsString("Неправильный логин. Может быть не менее 3 и не более 20 символов"));
    }

    @Test
    void negativePostLoginTest2() {
        final String response =
                given()
                        .formParam("username", username)
                        .formParam("password", "")
                        .log()
                        .all()
                        .when()
                        .post(loginURL)
                        .prettyPeek()
                        .then()
                        .assertThat()
                        .statusCode(200)
                        .extract()
                        .asString();
        assertThat(response, containsString("Неправильный логин. Может быть не менее 3 и не более 20 символов"));
    }

    public PostLoginTest() {
        super();
    }
}
